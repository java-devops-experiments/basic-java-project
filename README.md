# Basic Java Project

This project is a simple Java project only meant to be a base to
more advanced Devops and Gitlab testing.

So far, it contains:
- A functional Gitlab CI in two stages
- JUnit tests properly displayed in Gitlab
- A code coverage report on MR
- Conditional job system to run always, or only on MR/master, or for scheduled pipelines