import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.nicolas.Main.*;

public class MainTest {
    @Test
    public void equalTest() {
// Checks that the boolean condition is true.
        assertEquals(12, doStuff());
        System.out.println("Test passed!");
    }

    @Test
    public void runTest() {
// Checks that the boolean condition is true.
        main(null);
        System.out.println("Test passed!");
    }

    @Test
    public void runMoreTest() {
// Checks that the boolean condition is true.
        assertEquals(1, doMoreStuff());
        System.out.println("Test passed!");
    }
}
