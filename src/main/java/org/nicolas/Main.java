package org.nicolas;

public class Main {
    /**
     * Does stuff at execution.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    /**
     * Does different stuff.
     * @return meaningless value
     */
    public static int doStuff() {
        System.out.println("Doing stuff!");
        return 12;
    }

    /**
     * Does more stuff.
     * @return meaningless value
     */
    public static int doMoreStuff() {
        System.out.println("Doing more stuff!");
        return 1;
    }

    /**
     * Does unsafe stuff.
     * @return meaningless value
     */
    public static int doMoreUntestedStuff() {
        System.out.println("Doing unsafe stuff!");
        return 1;
    }
}
